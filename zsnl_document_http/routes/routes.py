# AUTO GENERATED FILE - DO NOT EDIT!
# GENERATED AT: 2019-05-14T14:46:20"
# Re-create by running 'generate-routes --package-name={package_name}'
# Run 'generate-routes --help' for more options


def add_routes(config):
    """Add routes to pyramid application.

    :param config: pyramid config file
    :type config: Configurator
    """
    handlers = [
        {
            "route": "/api/v2/document/create_document",
            "handler": "create_document",
            "method": "POST",
            "view": "zsnl_document_http.views.create_document.create_document",
        }
    ]

    for h in handlers:
        config.add_route(h["handler"], h["route"])
        config.add_view(
            view=h["view"],
            route_name=h["handler"],
            renderer="json",
            request_method=h["method"],
        )
