# AUTO GENERATED FILE - DO NOT EDIT!
# GENERATED AT: 2019-05-14T14:46:20"
# Re-create by running 'generate-routes --package-name={package_name}'
# Run 'generate-routes --help' for more options


from unittest import TestCase, mock

import webtest
from zsnl_document_http import main


class TestRoutesCreate_document(TestCase):
    def setUp(self):
        from zsnl_document_http.views import create_document

        self.patched_create_document = mock.patch(
            "zsnl_document_http.views.create_document", spec=create_document
        )
        self.create_document = self.patched_create_document.start()

        settings = {
            "minty_service.infrastructure.config_file": "tests/data/config.conf"
        }
        app = main({}, **settings)
        self.mock_app = webtest.TestApp(app)

    def tearDown(self):
        self.patched_create_document.stop()

    def test_routes_create_document(self):
        self.create_document.create_document.return_value = 200
        self.mock_app.post("/api/v2/document/create_document", status=200)
